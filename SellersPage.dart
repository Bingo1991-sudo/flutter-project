import 'package:flutter/material.dart';
import 'package:vegook/models/SallerModel.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:vegook/components/NavigationService.dart';
import 'package:vegook/screens/SallerScreen.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vegook/components/ViewPort.dart';
import 'package:vegook/states/SallerFavoriteState.dart';
import 'package:vegook/view/general/BackendConnectionServiceWidget.dart';
import 'package:vegook/exceptions/BackendConnectorServiceException.dart';

class SellersPage extends StatefulWidget {
  SellersPage({Key? key}) : super(key: key);

  @override
  State<SellersPage> createState() => _SellersPageState();
}

class _SellersPageState extends State<SellersPage> {
  TextStyle articleStyle = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 16.0,
    color: Color(0xff8a8a8a),
    fontFamily: 'SF Pro Display',
  );

  TextStyle profileNameTextStyle = TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 18.0,
    fontFamily: 'SF Pro Display',
  );

  @override
  Widget build(BuildContext context) {
    return Container(
        width: ViewPort.isTablet() ? 710 : null,
        padding: EdgeInsets.only(top: 78),
        child: FutureBuilder(
          future: SallerFavoriteState.getFavotites(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return new Center(child: new CircularProgressIndicator());
            }

            if (snapshot.hasError) {
              return BackendConnectionServiceWidget(             
                  exception:
                      snapshot.error as BackendConnectionServiceException);
            }

            var data = snapshot.data as List<SallerModel>;
            return ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, index) {
                var image;
                if (data[index].avatar.isNotEmpty)
                  image = NetworkImage(
                      'https://api.vegook.com/img/crop/132x132/avatars/${data[index].avatar}');
                else
                  image = AssetImage('assets/images/avatar.png');
                return Container(
                    margin: EdgeInsets.only(bottom: 22),
                    child: Wrap(children: [
                      GestureDetector(
                          onTap: () {
                            NavigationService.instance.navigateToRoute(
                                MaterialPageRoute(builder: (_) {
                              return SallerScreen(
                                sellerID: data[index].id,
                              );
                            }));
                          },
                          child: Container(
                            color: Theme.of(context).scaffoldBackgroundColor,
                            child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(78),
                                        border: Border.all(
                                            width: 1,
                                            color: Color(0xffb6b8b9),
                                            style: BorderStyle.solid),
                                      ),
                                      height: 56,
                                      width: 56,
                                      margin: const EdgeInsets.only(
                                          right: 0, top: 0, left: 15.0),
                                      child: Padding(
                                          padding: EdgeInsets.all(2),
                                          child: CircleAvatar(
                                              radius: 30,
                                              backgroundImage: image)),
                                    ),
                                    Container(
                                        margin: const EdgeInsets.only(
                                            right: 15.0, top: 0, left: 16.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 8),
                                                child: Text(data[index].name,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style:
                                                        profileNameTextStyle)),
                                            Text(
                                                (data[index].active +
                                                    ' ' +
                                                    AppLocalizations.of(
                                                            context)!
                                                        .add_count),
                                                style: this.articleStyle),
                                          ],
                                        ))
                                  ]),
                              Container(
                                  margin: EdgeInsets.only(right: 27),
                                  child: SvgPicture.asset(
                                      'assets/images/svg/arrow-to-seller.svg',
                                      height: 18,
                                      width: 9))
                            ],
                          )))
                    ]));
              },
            );
          },
        ));
  }
}
