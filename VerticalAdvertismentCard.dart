import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vegook/components/ViewPort.dart';
import 'package:vegook/models/AdvertisementCart.dart';
import 'package:vegook/screens/AdvertisementScreen.dart';
import 'package:vegook/states/AdvertisementFavoriteState.dart';
import 'package:vegook/theme/config.dart';
import 'package:vegook/view/catalog/ImgIcon.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:vegook/components/DateAndTime.dart';
import 'package:intl/intl.dart';

class VerticalAdvertismentCard extends StatefulWidget {
  final AdvertisementCart advertisement;

  VerticalAdvertismentCard({Key? key, required this.advertisement}) : super(key: key);

  @override
  _VerticalAdvertismentCardState createState() => _VerticalAdvertismentCardState();
}

class _VerticalAdvertismentCardState extends State<VerticalAdvertismentCard> {
  bool _toggleFavorite = false;

  TextStyle milageStyle = TextStyle(
    fontFamily: 'Rubik',
    fontSize: 16,
    fontWeight: FontWeight.w400,
    color: Color.fromRGBO(247, 250, 253, 1),
  );

  TextStyle titleStyle = TextStyle(
    fontFamily: 'SF Pro Display',
    fontSize: 16,
    fontWeight: FontWeight.w500,
		color: currentTheme.isLight() ? Color(0xFF1c1d39) : Color(0xFFFFFFFF),
  );

  TextStyle moneyStyle = TextStyle(
    fontFamily: 'SF Pro Display',
    fontSize: 18,
    fontWeight: FontWeight.w700,
    color: Color.fromRGBO(246, 103, 67, 1),
  );

  TextStyle placementStyle = TextStyle(
    fontFamily: 'SF Pro Display',
    fontSize: 14,
    fontWeight: FontWeight.w400,
    color: Color.fromRGBO(138, 138, 138, 1),
  );

  /// Клик по сердечку, чтобы добавить в избранные
  void toggleFavorite() {
    setState(() {
      _toggleFavorite = !_toggleFavorite;
    });
  }

  /// Главная картинка объявления
  Widget buildAdvertismentImage() {
    return Container(
      width: ViewPort.getWidth() / 2,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(12)),
        child: Column(children: [Expanded(child: Container(child: ImgIcon(txt: widget.advertisement.images[0].toString())))]),
      ),
    );
  }

  var formatter = NumberFormat('#,###,000');

  /// Переключатель "нравится"
  Widget buildFavoriteToggle() {
    return Positioned(
      right: 0,
      top: 0,
      child: IconButton(
        splashRadius: 38,
        iconSize: 38,
        icon: SvgPicture.asset(
          (_toggleFavorite) ? 'assets/images/svg/Like_active.svg' : 'assets/images/svg/Like_no_active.svg', height: (_toggleFavorite) ?38:36, color: Color(0xffFF6160),
        ),
        onPressed: () => AdvertisementFavoriteState.toggleFavorite(widget.advertisement.id, toggleFavorite, toggleFavorite),
      ),
    );
  }

  /// Рисует расстояние от пользователея до объявления
  Widget buildAdvertismentDistance() {
    return Positioned(
      left: 14,
      bottom: 15,
      child: Visibility(
          //todo: нам правда нужна вообще такая проверка?
          //мы что не должны показывать дистанцию, если она рядом? ex: "Рядом", вместо "0 km"
          visible: widget.advertisement.geo != '0',
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(99),
              color: Color(0xff181B22),
            ),
            child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                child: Text(widget.advertisement.geo + ' ' + AppLocalizations.of(context)!.km, style: milageStyle)),
          )),
    );
  }

  /// Рисует кружочки для списка кружочков, которые показывают сколько всего картинок у объявления
  Widget getImageCounterItem() {
    return Container(
        margin: const EdgeInsets.symmetric(horizontal: 2),
        child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(18)), child: SizedBox(width: (ViewPort.isTablet() || ViewPort.isMiddle()) ? 8 : 4, height: (ViewPort.isTablet() || ViewPort.isMiddle()) ? 8 : 4, child: Container(color: Color(0XFFFFFFFF)))));
  }

  /// Возвращает строку круглешочков, которые показывают сколько всего картинок у объявления
  Widget getImageCounterItemList() {
    List<Widget> circles = [];
    for (var i = 0; i < widget.advertisement.images.toList().length && i < 5; i++) {
      circles.add(getImageCounterItem());
    }
    return Row(mainAxisAlignment: MainAxisAlignment.spaceAround, crossAxisAlignment: CrossAxisAlignment.center, children: circles);
  }

  /// Это виджет, который рисует на карточке круглешочки,
  /// показывающие сколько всего у объявления картинок
  Widget buildAdvertismentImagesCount() {
    return Visibility(
        visible: (widget.advertisement.images.toList().length > 1),
        child: Positioned(
            right: 10,
            bottom: 12,
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(18)),
                child: SizedBox(
                    height: (ViewPort.isTablet() || ViewPort.isMiddle()) ? 16 : 10,
                    child: Container(
                      color: Color(0XFFFFFFFF).withOpacity(0.25),
                      padding: const EdgeInsets.symmetric(horizontal: 4),
                      child: getImageCounterItemList(),
                    )))));
  }

  /// Вспомогательный виджет, чтобы не отрисовывать строку с одинаковой версткой каждый раз в новом контейнере класса
  Widget someDescriptionTextRow(String text, TextStyle style) {
    return Align(alignment: Alignment.bottomLeft, child: Text(text, maxLines: 1, overflow: TextOverflow.ellipsis, textAlign: TextAlign.left, style: style));
  }

  /// Название объявления
  Widget buildAdvertismentTitle() {
    return Container(height: 31,
      child: someDescriptionTextRow(
          widget.advertisement.name.replaceAll(
              RegExp(
                  r'(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])'),
              ''),
          titleStyle),
    );
  }

  /// Цена объявления
  Widget buildAdvertismentPrice() {
    return Padding(
        padding: const EdgeInsets.only(top: 6),
        child: someDescriptionTextRow(formatter.format(widget.advertisement.cost).toString().replaceAll(',', ' ') + ' ₩', moneyStyle));
  }

  /// Город, указанный в объявлении
  Widget buildCurrentCity() {
    return Container(margin: EdgeInsets.only(top: 0), height: 20,
      child: someDescriptionTextRow(widget.advertisement.getCurrentCity(), placementStyle),
    );
  }

  /// Дата подачи объявления
  Widget buildAdvertismentDate() {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: someDescriptionTextRow(
          DateAndTime.getLocalDateFormat(
                      DateAndTime.toCurrentTimeZone(widget.advertisement.date))
                  .toString() +
              ', ' +
              DateAndTime.getFormatedTime(
                  DateAndTime.toCurrentTimeZone(widget.advertisement.date)),
          placementStyle),
    );
  }

  /// Собирает подготовленные заранее видежты, как конструктор,
  /// чтобы собрать верхнюю половину карточки
  Widget buildAvatarAndItElements() {
    List<Widget> cardUpBlocks = [
      buildAdvertismentImage(),
      buildFavoriteToggle(),
      buildAdvertismentDistance(),
      buildAdvertismentImagesCount(),
    ];
    return Expanded(child: Stack(children: cardUpBlocks));
  }

  /// Собирает подготовленные заранее видежты, как конструктор,
  /// чтобы собрать нижнюю половину карточки
  List<Widget> buildCardElements() {
    List<Widget> cardBDownlocks = [
      // Верхний блок - главная картинка объявления и ее элементы
      buildAvatarAndItElements(),

      // Нижний блок - Название объявления и всякие атрибуты
      buildAdvertismentTitle(),
      buildAdvertismentPrice(),
      buildCurrentCity(),
      buildAdvertismentDate(),
    ];
    return cardBDownlocks;
  }

  /// Позволяет нажать на карточку объявления и перейти 
  /// в соответсвующий `AdvertisementScreen`
  void gestureDetectorTap() {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => AdvertisementScreen(
        refreshFavoriteIcon: toggleFavorite,
        advertisementId: widget.advertisement.id,
      ),
    ));
  }

  @override
  void initState() {
    super.initState();
    _toggleFavorite = AdvertisementFavoriteState.isFavorite(widget.advertisement.id);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: gestureDetectorTap,
      child: Container(
				color: currentTheme.isLight() ? Color(0xFFFFFFFF) : Color(0xFF010101),
        width: MediaQuery.of(context).size.width / 2 - 8,
        child: Column(children: buildCardElements()),
      ),
    );
  }
}
