import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vegook/view/errors/Problems.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class GeneralProblem extends StatelessWidget {
  GeneralProblem({
    Key? key,
    this.action,
  }) : super(key: key);

  final dynamic action;

  @override
  Widget build(BuildContext context) {
    return Problems(
        error: AppLocalizations.of(context)!.oops,
        text: AppLocalizations.of(context)!.opps_text,
        image: 'oops.svg',
        btnText: AppLocalizations.of(context)!.to_home,
        showActionButton: true,
        action: action);
  }
}
